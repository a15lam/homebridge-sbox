var Service, Characteristic, HomebridgeAPI;
const request = require('request');

module.exports = function (homebridge) {
    HomebridgeAPI = homebridge;
    Service = homebridge.hap.Service;
    Characteristic = homebridge.hap.Characteristic;
    homebridge.registerAccessory("homebridge-sbox", "HomebridgeSbox", HomebridgeSbox);
};

function HomebridgeSbox(log, config) {
    this.log = log;
    this.ip = config["ip"];
    this.name = config["name"];
    this.onUrl = 'http://' + this.ip + '/switch/on';
    this.offUrl = 'http://' + this.ip + '/switch/off';
    this.stateUrl = 'http://' + this.ip + '/switch/state';
    this.cacheDir = HomebridgeAPI.user.persistPath();
    this.storage = require('node-persist');
    this.storage.initSync({dir:this.cacheDir, forgiveParseErrors:true});

    this.informationService = new Service.AccessoryInformation();
    this.informationService
        .setCharacteristic(Characteristic.Manufacturer, "ACube")
        .setCharacteristic(Characteristic.Model, "SBOX")
        .setCharacteristic(Characteristic.SerialNumber, this.ip.replace(".", ""));

    this.switchService = new Service.Switch(this.name);
    this.switchService.getCharacteristic(Characteristic.On)
        .on('get', this.getSwitchState.bind(this))
        .on('set', this.setSwitchState.bind(this));

    var cachedState = this.storage.getItemSync(this.name);
    if(cachedState === undefined || cachedState === false){
        this.switchService.setCharacteristic(Characteristic.On, false);
    } else {
        this.switchService.setCharacteristic(Characteristic.On, true);
    }
}

HomebridgeSbox.prototype.makeHttpRequest = function(url, next) {
    var me = this;
    request({
            url: url,
            method: 'GET'
        },
        function(error, response, body) {
            if (response.statusCode !== 200){
                me.log('Problem with SBOX switch: ' + me.name)
                me.log('STATUS: ' + response.statusCode);
                me.log('HEADERS: ' + JSON.stringify(response.headers));
                me.log('BODY: ' + body);
            }
            return next(error, response, body);
        }
    );
};

/**
 * This is called when Home opens.
 * If the switch state is changed manually or by other apps,
 * then this will update the switch state in Home app.
 *
 * @param {*} next
 */
HomebridgeSbox.prototype.getSwitchState = function(next) {
    var me = this;
    this.makeHttpRequest(this.stateUrl, function(error, response, body){
        if(error){
            return next(error);
        } else {
            var currentState = !!JSON.parse(body).switch;
            me.log('Current state: ' + currentState);
            me.storage.setItemSync(me.name, currentState);
            return next(null, currentState);
        }
    });
};

/**
 * This is called when Switch is turned on/off
 *
 * @param {*} state
 * @param {*} next
 */
HomebridgeSbox.prototype.setSwitchState = function(state, next) {
    var me = this;
    var setUrl = state? this.onUrl : this.offUrl;
    this.makeHttpRequest(setUrl, function(error, response, body){
        if(error){
            return next(error);
        } else {
            var currentState = !!JSON.parse(body).switch;
            me.log('Set state: ' + currentState);
            me.storage.setItemSync(me.name, currentState);
            return next();
        }
    });
};

HomebridgeSbox.prototype.getServices = function() {
    return [this.informationService, this.switchService];
};
